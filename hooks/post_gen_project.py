import os

types = set(["basic", "fighter"])
bot_type = "{{cookiecutter.bot_type}}"
types.remove(bot_type)
for unused in types:
    os.remove(f"{unused}.py")

os.rename(f"{bot_type}.py", "bot.py")
