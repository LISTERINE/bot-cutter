How to use:

Until cookiecutter 2.0 is out, you'll need to use the version at this commit:

`pip install git+git://github.com/cookiecutter/cookiecutter.git@623e4b9f03b36bcd0c69acc52046cc4d98baac41`


then cut the project with:

`cookiecutter https://gitlab.com/LISTERINE/bot-cutter`
