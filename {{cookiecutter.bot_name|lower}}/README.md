http://discordapp.com/developers/applications
add application
create bot
oauth2
        - bot from the SCOPES options
        - Administrator from BOT PERMISSIONS
        - copy oauth url, open in browser, sign in, join guild

create k8s secret:
kubectl create secret generic discord-{{cookiecutter.bot_name|lower}} --from-literal=discord-token='{get token from discord}'
kubectl apply -f k8s_bot.yaml
