import random

from discord_base_bots.base import run_bot
from discord_base_bots.fighter import Fighter


class {{cookiecutter.__class_name}}(Fighter):

    bot_name = "{{cookiecutter.bot_name}}"
    hot_word = "{{cookiecutter.hot_word}}"

    #############################
    #
    # Available data:
    # self.opponent: str: your opponents name
    # self.current_health: int: your current health (don't adjust this, that's cheating)
    # self.current_endurance: int: your current endurance (don't adjust this, that's cheating)
    # self.advantage:self.advantage: bool: If you had the first round advantage
    #
    #############################

    #############################
    #
    # Your round actions
    #
    #############################
    def pre_action(self, *args):
        """Make a statement before your round action.

        Does not impact the battle, just for fun.

        Returns:
            Optional(str|list[str]): A string or list of strings to state before your round action.
        """
        pass

    def action(self, *args):
        """What do you want to do this round?

        To attack: include the word attack, delimited by whitespace, in your return string.
        To defend: include the word defend, delimited by whitespace, in your return string.

        You *MUST* include attack or defend in your action string.

        Returns:
            str: Your round action string with the required keyword in it.
        """
        return random.choice([":crossed_swords: attack :crossed_swords:", ":shield: defend :shield:"])

    def read_reaction(self, reaction):
        """Read how your opponent reacted to *YOUR* round action.

        Does not impact battle.

        Give your bot a chance to react to -your opponents reaction to *your* round action.-

        For example:
            You attack
            Your opponent is defending and response by letting you know they blocked the attack
            _this method_ Read that they blocked your attack and offer a reaction string to animate the fight.

        Returns:
            Optional(str|list[str]): A reaction string in response to the action your opponent took this round.
        """
        if f"{self.opponent} blocks" in reaction:
            return f"{self.hot_word} staggers back"

    #############################
    #
    # Opponent round actions
    #
    #############################
    def process_opponent_action(self, action):
        """View the action your opponent took.

        Args:
            action (str): The action your opponent took this round.

        Returns:
            Optional[str]: A text response to the action.
        """
        if action.endswith("defend"):
            return "_searches your defence_"
        elif action.endswith("attack"):
            return "_attempts to counter_"

    #############################
    #
    # End conditions
    #
    #############################
    def on_victory(self, data, message):
        """Message(s) to send when you win.

        Returns:
            Optional(str|list[str]):
        """
        return ["_throws down weapon_",
                "You were no match."]

    def on_loss(self, data, message):
        """Message(s) to send when you run out of health.

        Returns:
            Optional(str|list[str]): Message(s) to send when you run out of health.
        """
        return "I've failed."

    def on_timeout(self, data, message):
        """Message(s) to send when you run out of endurance.

        Returns:
            Optional(str|list[str]): Message(s) to send when you run out of endurance.
        """
        return "I can't go on."


if __name__ == "__main__":
    from pathlib import Path
    # Enable logger
    # import logging
    # logging.getLogger("discord-base-bots").addHandler(logging.StreamHandler())
    # logging.basicConfig()

    env_file = Path(".env").resolve()

    if env_file.exists():
        run_bot({{cookiecutter.__class_name}}, env_file)
    else:
        run_bot({{cookiecutter.__class_name}})
