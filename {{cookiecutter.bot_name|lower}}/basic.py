from discord_base_bots.base import BaseBot, run_bot


class {{cookiecutter.__class_name}}(BaseBot):

    bot_name = "{{cookiecutter.bot_name}}"
    hot_word = "{{cookiecutter.hot_word}}"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # command word: function object
        # "roll": self.roll
        self.commands = {
        }

    async def on_ready(self):
        print(f'{self.bot_name} online')

    async def on_message(self, message):
        if message.content.lower().startswith(self.hot_word):
            void, command_and_data = message.content.split(sep=None, maxsplit=1)
            command = None
            command_content = None
            for command_name, command_func in self.commands.items():
                if command_and_data.lower().startswith(command_name):
                    command = command_func
                    command_content = command_and_data.split(command_name, maxsplit=1)[-1].strip()
                    break
            if command is not None:
                await command(command_content, message)


if __name__ == "__main__":
    from pathlib import Path
    run_bot({{cookiecutter.__class_name}}, Path(".env").resolve())
